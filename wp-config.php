<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpressAPI' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Uen|aA0pI>6QJSl8cQVpC7;-TWU#|v7dI;<dsuZ H)S,Xgc-%-M~?&~!MH^C2q83' );
define( 'SECURE_AUTH_KEY',  'H>F3u*lPh]~s)w`4mHU$I248FA~eI&Aojlx2TWy;wv7,eQ^lXt}ub{ny6uL&m#ub' );
define( 'LOGGED_IN_KEY',    '^aaYz<^_]SaOx#=yDjIPM_w0#Iz2hSyF3salZl4uXPDfZR4ueQzPw%kdDVW]D}M`' );
define( 'NONCE_KEY',        '+-DDw{9No?X`}`j%]F8~b-%4BEhN&VGNX?0Avl$gzrRvaE;YjD_ZX1.?i/[fvUYK' );
define( 'AUTH_SALT',        '#Ihd+lhT=%KRtB4w_%h:fVK~2T|K>AIiIKL|jF$W&r.H[Z,Iw^8}~^+a*S:+o^4;' );
define( 'SECURE_AUTH_SALT', 'D&-19N5vs|]8j*7TchogEw=*^2UQoCU|c=4cw|tn%t<,DewfV8Iz!41@ea4}-YJh' );
define( 'LOGGED_IN_SALT',   '3b.Xxa)ZCERJaE>rlDuv($}H7H34EgOE*`7YjK.A>%qN#~BW8[oIM!v,#9/|RqMl' );
define( 'NONCE_SALT',       'NfN*RFG6q_b$JAJW+u=4nGC&r,mSn4${4n?K4Ja}CAPEE#R2AynB1Hm5w)njIdj7' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
