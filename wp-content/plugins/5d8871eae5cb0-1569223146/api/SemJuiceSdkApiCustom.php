<?php

/**
 * Class SemJuiceSdkApiCustom
 *
 * Exemple d'intégration
 *
 * En considérant les tables suivantes :
 *
    CREATE TABLE `article` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `title` varchar(150) DEFAULT NULL,
        `content` text,
        `article_status` tinyint(1) unsigned NOT NULL DEFAULT '1', -- 1 = valid | 0 = draft
        `category_id` smallint(5) unsigned NOT NULL DEFAULT '0', -- category table
        `security_semjuice` int(10) unsigned DEFAULT NULL, -- securty id semjuice
        `image_main` varchar(200) DEFAULT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `security_semjuice` (`security_semjuice`),
        KEY `category_id` (`category_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    CREATE TABLE `category` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `title` varchar(150) NOT NULL DEFAULT '',
        `id_parent` int(10) unsigned NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        KEY `id_parent` (`id_parent`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */
class SemJuiceSdkApiCustom extends SemjuiceSdkApi
{
    const API_KEY = '';
    const API_SECRET = '';

    const DIRECTORY_IMAGE = '/http/www/images/';
    const URL_IMAGE = '//www.example.com/images/';

    const DB_PASS = '';
    const DB_USER = '';
    const DB_NAME = '';
    const DB_SERV = '';

    private $dbSave;

    protected function init()
    {
        $this
            ->setApiKey(self::API_KEY)
            ->setApiSecret(self::API_SECRET);
    }

    private function db()
    {
        if ($this->dbSave !== null) {
            return $this->dbSave;
        }

        // \PDO::MYSQL_ATTR_INIT_COMMAND = 1002
        $driver_options = array(1002 => "SET NAMES utf8");

        return ($this->dbSave = new PDO(
            'mysql:host=' . self::DB_SERV . ';dbname=' . self::DB_NAME,
            self::DB_USER,
            self::DB_PASS,
            $driver_options
        ));
    }

    //----------------------------------------------------------------------------\\
    //                               ABSTRACT METHODS
    //----------------------------------------------------------------------------//

    /**
     * Nous permet de poster un article sur votre site
     *
     * @param array $parameters format :
     *                          [
     *                              'inner_id' => '', id de sécurité de SEMJuice (à associer à vos articles)
     *                              'article_status' => '', // 1 = article valide | 0 = article brouillon
     *                              'title' => '', // titre de l'article
     *                              'html' => '', // contenu (html) de l'article
     *                              'category' => '', // id de vos catégories (communiqué via $this->categoryGet())
     *                              'image_main' => [ // image principale
     *                                  'file' => '', // url de l'image (à télécharger sur votre serveur !)
     *                                  'id' => '', // id de l'image SEMJuice
     *                              ],
     *                              'image_second' => [ // imageS présentes dans le code HTML
     *                                  [
     *                                      'file' => '', // url de l'image à la taille exacte (à télécharger sur votre serveur !)
     *                                      'file_source' => '', // url de l'image à la taille d'origine
     *                                      'id' => '', // id de l'image SEMJuice
     *                                      'current_uri' => '', // chemin de l'image présent dans le code HTML (à remplacer par le votre)
     *                                  ]
     *                                  ..
     *                              ],
     *                          ]
     *
     * @return array
     */
    protected function articleAdd($parameters)
    {
        // security_semjuice > permet de vous assurer que nous ne puissions modifier que les articles SEMJuice (option)
        $statment = $this->db()->prepare(
            'INSERT INTO article 
            (title, content, article_status, category_id, security_semjuice)
            VALUES
            (:title, :content, :article_status, :category_id, :security_semjuice)
            '
        );
        $statment->execute(array(
            'title'             => $parameters['title'],
            'content'           => $parameters['html'],
            'article_status'    => $parameters['article_status'], // 1 = valid | 0 = draft
            'category_id'       => $parameters['category'],
            'security_semjuice' => $parameters['inner_id'],
        ));

        $article_id = $this->db()->lastInsertId();

        $this->articleImage($parameters, $article_id);

        return array(
            'status' => 'success',
            'data' => array(
                'id' => $article_id,
                'url' => 'http://www.example.com/article-' . $article_id
            )
        );
    }

    /**
     * Nous permet de modifier un article déjà posté
     *
     * @param array $parameters pour une question de sécurité, nous modifions uniquement les articles
     *                          avec l'id de sécurité semjuice ['inner_id' => ''], il est donc indispensable
     *                          que vous associez l'id de semjuice à vos articles.
     *
     *                          il contiendra également les données de l'article (identique à articleAdd)
     *
     * @return array
     */
    protected function articleUpdate($parameters)
    {
        $data = $this->articleFind(array('inner_id' => $parameters['inner_id']));

        if (!isset($data['status'])
            || (isset($data['status']) && $data['status'] == 'error')
            || !isset($data['data']['id'])
            || !$data['data']['id']
        ) {
            return array(
                'status' => 'error',
                'data' => $this->errorMessage['article_not_exist']
            );
        }

        $article_id = $data['data']['id'];

        // update article
        $statment = $this->db()->prepare(
            'UPDATE article
            SET 
                title = :title,
                content = :content,
                article_status = :article_status,
                category_id = :category_id
            WHERE
                id = :id
            '
        );
        $success = $statment->execute(array(
            'title'             => $parameters['title'],
            'content'           => $parameters['html'],
            'article_status'    => $parameters['article_status'], // 1 = valid | 0 = draft
            'category_id'       => $parameters['category'],
            'security_semjuice' => $parameters['inner_id'],
            'id'                => $article_id,
        ));

        $this->articleImage($parameters, $article_id);

        return array(
            'success' => $success ? 'success' : 'error'
        );
    }

    /**
     * Nous permet de retrouver un article
     *
     * @param array $parameters peut contenir :
     *                          soit l'url du lien partenaire, ['url' => '']
     *                          soit l'id de votre article, ['article_id' => '']
     *                          soit l'id de sécurité SEMJuice (nous permettant d'agir ['inner_id' => '']
     *                              uniquement sur les articles ajoutés par SEMjuice
     *
     * @return array
     */
    protected function articleFind($parameters)
    {
        $where[] = '';
        $whereParameters = array();
        if (isset($parameters['url']) && $parameters['url']) {
            $where[] = 'content LIKE ?';
            $whereParameters[] = '%' . $parameters['url'] . '%';
        }
        if (isset($parameters['article_id']) && $parameters['article_id']) {
            $where[] = 'id = ?';
            $whereParameters[] = $parameters['article_id'];
        }
        if (isset($parameters['inner_id']) && $parameters['inner_id']) {
            $where[] = 'security_semjuice = ?';
            $whereParameters[] = $parameters['inner_id'];
        }

        if (!$where || !$whereParameters) {
            return array(
                'status' => 'error',
                'data' => $this->errorMessage['article_not_exist']
            );
        }

        $whereSearch = implode(' AND ', $where);

        $statment = $this->db()->prepare(
            'SELECT id, article_status FROM article WHERE ' . $whereSearch
        );
        $statment->execute($whereParameters);
        $article = $statment->fetch(PDO::FETCH_ASSOC);

        if (!isset($article['id'])) {
            return array(
                'status' => 'error',
                'data' => $this->errorMessage['article_not_exist']
            );
        }

        return array(
            'status' => 'success',
            'data' => array(
                'id'             => $article['id'],
                'url'            => 'http://www.example.com/article-' . $article['id'],
                'article_status' => $article['article_status'] // 1 = valid | 0 = draft
            )
        );
    }

    /**
     * Nous retourne la liste de vos catégories et sous catégories
     *
     * @return array format :
     *               [
     *                  [
     *                      'id' => 1,
     *                      'title' => 'Titre catégorie',
     *                      'id_parent' => 2
     *                  ],
     *                  ...
     *               ]
     *               id_parent étant optionnel (si vous n'avez pas de sous catéogire, inutile de le remplir)
     */
    protected function categoryGet()
    {
        $statment = $this->db()->prepare(
            'SELECT id, title, id_parent FROM category'
        );
        $statment->execute();

        return array(
            'status' => 'success',
            'data' => $statment->fetchAll(PDO::FETCH_ASSOC)
        );
    }

    /**
     * Des informations vous seront envoyées de temps en temps, vous pouvez les stocker et les afficher si vous le souhaitez
     *
     * @param array $parameters format :
     *                          [
     *                              'data' => [
     *                                  'identifiant_unique' => [
     *                                      'title' => 'Titre de l\'information',
     *                                      'value' => 'Contenu de l\'information',
     *                                  ]
     *                                  ...
     *                              ]
     *                          ]
     */
    protected function infoUpdate($parameters)
    {
        file_put_contents('file_path_semjuice.txt', $parameters['info']);
    }

    //----------------------------------------------------------------------------\\
    //                               HELPERS
    //----------------------------------------------------------------------------//

    protected function articleImage($parameters, $articleId)
    {
        if (isset($parameters['image_main']) && $parameters['image_main']) {
            $this->imageMainAdd($parameters, $articleId);
        }

        if (isset($parameters['image_second']) && $parameters['image_main']) {
            $this->imageSecondAdd($parameters, $articleId);
        }
    }

    protected function imageMainAdd($parameters, $articleId)
    {
        $imageName = basename($parameters['image_main']['file']);
        $imagePath = self::DIRECTORY_IMAGE . $imageName;
        $imageUrl  = self::URL_IMAGE . $imagePath;

        file_put_contents(
            $imagePath,
            file_get_contents($parameters['image_main']['file'])
        );

        // update article
        $statment = $this->db()->prepare(
            'UPDATE article
            SET 
                image_main = :image_main
            WHERE
                id = :id
            '
        );
        $statment->execute(array(
            'image_main' => $imageUrl,
            'id'         => $articleId,
        ));
    }

    protected function imageSecondAdd($parameters, $articleId)
    {
        $images = array();

        foreach ($parameters['image_second'] as $image) {
            $imageName = basename($image['file']);
            $imagePath = self::DIRECTORY_IMAGE . $imageName;
            $imageUrl  = self::URL_IMAGE . $imagePath;

            file_put_contents(
                $imagePath,
                file_get_contents($image['file'])
            );

            $images[$image['current_uri']] = $imageUrl;
        }

        $statment = $this->db()->prepare(
            'UPDATE article
            SET 
                content = :content
            WHERE
                id = :id
            '
        );
        $statment->execute(array(
            'id'      => $articleId,
            'content' => str_replace(array_keys($images), array_values($images), $parameters['html'])
        ));
    }
}
