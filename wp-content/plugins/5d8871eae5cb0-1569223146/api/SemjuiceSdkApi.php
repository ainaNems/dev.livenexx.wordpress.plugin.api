<?php

/**
 * Class SemjuiceSdkApi
 *
 * Il faut étendre cette classe, et paramètrer, selon votre développement,
 * les methodes utilisées par SEMJuice pour vous envoyer des données.
 * Voir "$receiveCommandAvailable" pour avoir la liste des méthodes en question.
 * Elles sont optionnelles (si vous ne renseignez pas "postNetlink" par exemple,
 * nous ne pourrons pas vous envoyer les articles sponsorisés automatiquement)
 *
 * list of errors to respect :
 * - 1 : Command not implemented
 * - 2 : Command bad implemented
 */
abstract class SemjuiceSdkApi
{
    protected $errorMessage = array(
        'command_not_implemented' => array(
            'id' => 1,
            'message' => 'Command not implemented'
        ),
        'command_bad_implemented' => array(
            'id' => 2,
            'message' => 'Command bad implemented'
        ),
        'article_not_add' => array(
            'id' => 3,
            'message' => 'Article not add'
        ),
        'article_not_exist' => array(
            'id' => 4,
            'message' => 'Article not exist'
        ),
        'article_update_security' => array(
            'id' => 5,
            'message' => 'Article update error : security'
        ),
        'signature' => array(
            'id' => 6,
            'message' => 'Signature false'
        )
    );

    const API_VERSION = '2.0';
    protected $apiKey;
    protected $apiSecret;
    protected $apiUri = 'https://www.semjuice.com/api/';
    protected $receiveCommandAvailable = array(
        'testAccess',
        'articleAdd',
        'articleSend',
        'articleUpdate',
        'articleFind',
        'categoryGet',
        'infoUpdate',
    );

    //----------------------------------------------------------------------------\\
    //                               SETTER
    //----------------------------------------------------------------------------//

    public function __construct($apiKey = null, $apiSecret = null, $endpoint = null)
    {
        $this
            ->setApiKey($apiKey)
            ->setApiSecret($apiSecret);

        if (method_exists($this, 'init')) {
            $this->init();
        }

        $receive = file_get_contents('php://input');
        $receive = $receive ? json_decode($receive, true) : array();
        $receive = is_array($receive) ? $receive : array();
        $receive = array_merge($_GET, $receive);

        if ($receive && isset($receive['nonce'], $receive['sign'], $receive['key'])) {
            // verify : is JSON / signature / command exist
            if (is_array($receive)
                && !empty($receive) // json ok
                && $this->signatureVerify($receive) === true // signature ok
                && isset($receive['command'])
                && in_array($receive['command'], $this->receiveCommandAvailable) // command ok
            ) {
                $this->receiveDispatch($receive);
            } else {
                throw new InvalidArgumentException(
                    "SEMJuice : receive is not valid json or not valid signature."
                );
            }
        } elseif (defined('API_SEMJUICE_SEE_MESSAGE') && API_SEMJUICE_SEE_MESSAGE == true) {
            echo 'Access done';
        }
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;

        return $this;
    }

    //----------------------------------------------------------------------------\\
    //                               SEND
    //----------------------------------------------------------------------------//

    public function apiVerify()
    {
        return $this->request(array('command' => 'apiVerify'));
    }

    public function siteEdit(array $parameters)
    {
        return $this->request(array_merge(array('command' => 'siteEdit'), array('parameters' => $parameters)));
    }

    //----------------------------------------------------------------------------\\
    //                               RECEIVE
    //----------------------------------------------------------------------------//


    protected function receiveDispatch(array $parameters)
    {
        if (!method_exists($this, $parameters['command'])) {
            $this->response(array(
                'status' => 'error',
                'data' => $this->errorMessage['command_not_implemented']
            ));
        }

        $response = call_user_func(array($this, $parameters['command']), $parameters);

        if (!is_array($response)
            || !isset($response['status'])
            || !in_array($response['status'], array('success', 'error'))
        ) {
            $this->response(array(
                'status' => 'error',
                'data' => $this->errorMessage['command_bad_implemented']
            ));
        }

        $this->response($response);
    }

    protected function testAccess()
    {
        return array(
            'status' => 'success',
            'data' => array(
                'version_api' => self::API_VERSION
            )
        );
    }
    
    abstract protected function articleSend($parameters);

    abstract protected function articleAdd($parameters);

    abstract protected function articleUpdate($parameters);

    abstract protected function articleFind($parameters);

    abstract protected function categoryGet();

    abstract protected function infoUpdate($parameters);

    //----------------------------------------------------------------------------\\
    //                               HELPERS
    //----------------------------------------------------------------------------//

    protected function request(array $parameters = array())
    {
        if (!isset($parameters['command']) || !$parameters['command']) {
            throw new Exception("Aucune commande dans les parametres");
        }

        $client = curl_init();

        $parameters['parameters'] = isset($parameters['parameters']) && is_array($parameters['parameters'])
            ? $parameters['parameters']
            : array();

        $dataCurl = array('command' => $parameters['command']) + $this->signature() + $parameters['parameters'];

        $parametersCurl = array(
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_AUTOREFERER    => true,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT        => 120,
            CURLOPT_FAILONERROR    => true,
            CURLOPT_URL            => $this->apiUri,
            CURLOPT_POSTFIELDS     => json_encode($dataCurl),
            CURLOPT_SSL_VERIFYPEER => 0, // osx : 1 / default : 0
            CURLOPT_SSL_VERIFYHOST => 0, // osx : 2 / default : 0
        );
        
        // var_dump($parametersCurl);

        // curl_setopt_array equivalent (bug PHP 5.5)
        foreach ($parametersCurl as $optionName => $value) {
            curl_setopt($client, $optionName, $value);
        }

        $response = curl_exec($client);
        $error = curl_error($client);
        curl_close($client);

        if (!$response) {
            return array(
                'status' => 'error',
                'data' => array(
                    'id' => 0,
                    'message' => 'SEMJuice not found, CURL response : ' . $error,
                )
            );
        }
        // var_dump($response);

        $array = json_decode($response, true);
        

        if (!is_array($array) || !isset($array['status'])) {
            return array(
                'status' => 'error',
                'data' => array(
                    'id' => 0,
                    'message' => 'Api RESPONSE unrecognized' . var_export($response, true),
                    'response' => $response,
                )
            );
        }

        return $array;
    }

    protected function response(array $datas = array())
    {
        header('Content-Type: application/json');
        exit(json_encode($datas));
    }

    protected function signature($forceNonce = null)
    {
        $nonce = $forceNonce !== null ? $forceNonce : time();

        return array(
            'sign'  => md5($nonce . $this->apiSecret),
            'key'   => $this->apiKey,
            'nonce' => $nonce
        );
    }

    protected function signatureVerify(array $parameters = array())
    {
        $signature = $this->signature($parameters['nonce']);

        if ($this->apiKey === $parameters['key']
            && $signature['sign'] === $parameters['sign']
        ) {
            return true;
        }

        $this->response(array(
            'status' => 'error',
            'data' => $this->errorMessage['signature']
        ));

        return false;
    }
}
