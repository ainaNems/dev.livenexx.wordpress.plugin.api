<?php
define('API_SEMJUICE_SEE_MESSAGE', true);
// wordpress load
$wpLoad = 'wp-load.php';
while (!is_file($wpLoad)) {
    if (is_dir('..')) {
        $wpLoad = '../' . $wpLoad;
    } else {
        exit(json_encode(array(
            'status' => 'error',
            'data' => array(
                'id' => 0,
                'message' => 'wp-load.php not found'
            )
        )));
    }
}
require $wpLoad;

// api for common
require 'SemjuiceSdkApi.php';

// api for WP
require '../inc/classes/semjuice-api-custom.php';

// run
new Semjuice_Api_Custom();
