<?php
if (!function_exists('add_action')) {
    // direct access > 404 ...
    header("HTTP/1.0 404 Not Found");
    exit;
}

class Semjuice_Admin
{
    private static $initiated = false;
    private static $infoDefault = array(
        'wordpress' => array(
            'info' => '',
            'mea' => 'error',
        ),
        'data' => array(
            'error_install' => array(
                'title' => 'Erreur',
                'value' => 'Votre site n\'est pour le moment pas relié à SEMJuice<br>
                    1 - Entrez la clé publique et secrète<br>
                    2 - Envoyer le formulaire via "Enregistrer les modifications"<br>
                    3 - En bas, entrez l\'ID de votre site et vérifier l\'URL de communication<br>
                    4 - Cliquez sur "Envoyer à SEMJuice", vous devriez avoir un message de confirmation d\'installation<br>'
            )
        )
    );

    public static function init()
    {
        if (!self::$initiated) {
            self::init_hooks();
        }
    }

    public static function init_hooks()
    {
        self::$initiated = true;

        add_action('admin_menu', array(__CLASS__, 'admin_menu')); // php 5.4 : not use self::class
    }

    public static function admin_menu()
    {
        ob_start(); // tip redirect ...

        $dirPlugin   = WP_PLUGIN_DIR . '/' . preg_replace('#(.+?)/inc/.*#i', '$1', plugin_basename(__FILE__));
        $info = get_option(SEMJUICE_PREFIX . 'info');

        if (!$info) {
            update_option(SEMJUICE_PREFIX . 'info', self::$infoDefault);
            $info = get_option(SEMJUICE_PREFIX . 'info');
        }

        $notif = isset($info['wordpress']['info']) && !empty($info['wordpress']['info'])
            ? ' <span style="font-size: 0.8em;">' . $info['wordpress']['info'] . '</span>'
            : '';

        $notif = isset($info['wordpress']['mea']) && !empty($info['wordpress']['mea'])
            ? ' <span class="awaiting-mod"><span>' . $info['wordpress']['mea'] . '</span></span>'
            : $notif;

        if (isset($info['wordpress']['majUrl'])
            && isset($info['wordpress']['majVersion'])
            && version_compare($info['wordpress']['majVersion'], SEMJUICE_VERSION, '>')
        ) {
            $notif = ' <span class="awaiting-mod"><span>maj</span></span>';
        }

        add_menu_page(
            'SEMJuice', // page title
            'SEMJuice' . $notif, // menu title
            'manage_options', // capability for see menu
            'semjuice-config', // name menu
            array(__CLASS__, 'config_page'), // function call
            'data:image/svg+xml;base64,' . base64_encode(file_get_contents($dirPlugin . '/inc/views/semjuice.svg'))
        );

        $info = get_option(SEMJUICE_PREFIX . 'info');

        if (isset($info['wordpress']['majUrl'])
            && isset($info['wordpress']['majVersion'])
            && version_compare($info['wordpress']['majVersion'], SEMJUICE_VERSION, '>')
        ) {
            // hack duplicate
            add_submenu_page(
                'semjuice-config', // parent
                '', // page title
                '', // menu title
                'manage_options', // capability for see menu
                'semjuice-config', // name menu
                array(__CLASS__, 'config_page') // function call
            );

            add_submenu_page(
                'semjuice-config', // parent
                __('Lancer la mise à jour'), // page title
                __('Lancer la mise à jour'), // menu title
                'manage_options', // capability for see menu
                'semjuice-maj', // name menu
                array(__CLASS__, 'maj_page') // function call
            );
        }
    }

    //----------------------------------------------------------------------------\\
    //                               PAGE
    //----------------------------------------------------------------------------//

    public static function maj_page()
    {
        $info = get_option(SEMJUICE_PREFIX . 'info');

        if (!isset($info['wordpress']['majUrl'])) {
            self::exitMessage('Erreur : url de MAJ inexistante, merci de contacter SEMJuice');
        }

        $majZip      = $info['wordpress']['majUrl'];
        $dirPlugin   = WP_PLUGIN_DIR . '/' . preg_replace('#(.+?)/inc/.*#i', '$1', plugin_basename(__FILE__));
        $pathMaj     = $dirPlugin . '/maj/';
        $pathMajFile = $pathMaj . 'majZip.zip';

        $chmodCurrent = fileperms($dirPlugin);
        self::chmodRecursive($dirPlugin, 0777);

        if (!is_dir($pathMaj)) {
            $mkdirSuccess = mkdir($pathMaj);

            if (!$mkdirSuccess) {
                self::exitMessage('Erreur : le dossier de votre plugin n\'accepte pas l\'écriture.');
            }
        }

        $contentZip = file_get_contents($majZip);

        if ($contentZip === false) {
            self::exitMessage('Erreur : le ZIP de mise à jour ne peut pas être récupéré.');
        }

        $zipPut = file_put_contents($pathMajFile, $contentZip);

        if ($zipPut === false) {
            self::exitMessage('Erreur : le ZIP de mise à jour ne peut pas être mis sur votre serveur.');
        }

        WP_Filesystem();

        $unzipfile = unzip_file($pathMajFile, $dirPlugin . '_maj/');

        self::deleteRecursive($dirPlugin . '/');
        self::copyRecursive($dirPlugin . '_maj/semjuice/', $dirPlugin . '/');
        self::chmodRecursive($dirPlugin, $chmodCurrent);

        if ($unzipfile) {
            self::exitMessage('La mise à jour a bien été effectué.');
        } else {
            self::exitMessage('Erreur : erreur inconnue, le dezippage du fichier de MAJ n\'a pas fonctionné.');
        }
        exit;
    }

    public static function config_page()
    {
        $datas = array(
            'api_key'          => null,
            'api_secret'       => null,
            'api_url'          => preg_replace('#(.*)inc/classes/$#si', '$1api/receive_wp.php', plugin_dir_url(__FILE__)),
            'post_author'      => null,
            'info'             => null,
            'site_id'          => null,
            'plugin_dir'       => preg_replace('#(.+?)/inc/.*#i', '$1', plugin_basename(__FILE__))
        );

        $isSubmit = isset($_POST['option_page'])
            && $_POST['option_page'] == SEMJUICE_NONCE_CONFIG
            && wp_verify_nonce($_POST['_wpnonce'], SEMJUICE_NONCE_CONFIG . '-options');

        $isSubmitSend = isset($_POST['option_page'])
            && $_POST['option_page'] == SEMJUICE_NONCE_CONFIG . '_send'
            && wp_verify_nonce($_POST['_wpnonce'], SEMJUICE_NONCE_CONFIG . '_send-options');

        if ($isSubmit
            && isset($_POST['plugin_dir'])
            && $_POST['plugin_dir']
            && preg_match('#[a-z0-9\-]+#i', $_POST['plugin_dir'])
            && $datas['plugin_dir'] != $_POST['plugin_dir']
        ) {
            // plugin name change
            self::pluginChangeName($datas['plugin_dir'], 'semjuice.php');

            delete_option(SEMJUICE_PREFIX . 'api_url');
            delete_option(SEMJUICE_PREFIX . 'info');

            wp_redirect(admin_url('admin.php?page=semjuice-config&namechange'));
        }

        if (isset($_GET['namechange'])) {
            $messageSuccess = 'Le nom du plugin a bien été modifié. 
                Si vous aviez déjà envoyé l\'url de lien entre SEMJuice et ce blog,
                n\'oubliez pas de l\'envoyer à nouveau pour nous signaler le changement d\'URL';
        }

        foreach ($datas as $fieldName => $data) {
            if ($fieldName == 'plugin_dir') {
                continue;
            }

            if (($isSubmit || $isSubmitSend) && isset($_POST[$fieldName])) {
                if ($_POST[$fieldName]) {
                    update_option(SEMJUICE_PREFIX . $fieldName, $_POST[$fieldName]);
                } else {
                    delete_option(SEMJUICE_PREFIX . $fieldName);
                }
            } elseif ($data !== null) {
                $dataRecord = get_option(SEMJUICE_PREFIX . $fieldName);

                if (!$dataRecord) {
                    update_option(SEMJUICE_PREFIX . $fieldName, $data);
                }
            }

            $datas[$fieldName] = get_option(SEMJUICE_PREFIX . $fieldName);
        }

        if ($isSubmit) {
            $messageSuccess = 'Vos configurations ont bien été mises à jour.';
        }

        if ($isSubmitSend) {
            $return = self::sendToSemjuice();

            if (isset($return['status']) && $return['status'] == 'success') {
                $messageSuccess = 'Votre API est bien configuré, félicitation.';

                $info = get_option(SEMJUICE_PREFIX . 'info');

                if ($info['wordpress']['mea'] == 'error') {
                    $info['wordpress']['mea'] = '';
                    unset($info['data']['error_install']);
                }

                $info['data']['setup'] = array(
                    'title' => 'Installation',
                    'value' => 'Votre plugin est correctement installé !'
                );

                update_option(SEMJUICE_PREFIX . 'info', $info);

                $datas['info'] = $info;

            } elseif ((isset($return['status']) && $return['status'] == 'error') || !isset($return['status'])) {
                $messageError = 'Une erreur s\'est produite : ' . (isset($return['data']['message']) ? $return['data']['message'] : 'erreur inconnue');
                $info = get_option(SEMJUICE_PREFIX . 'info');
                $newInfo = array_merge($info, self::$infoDefault);
                update_option(SEMJUICE_PREFIX . 'info', $newInfo);

                $datas['info'] = $newInfo;
            }
        }

        require SEMJUICE_PLUGIN_DIR . 'inc/views/config.php';
    }

    //----------------------------------------------------------------------------\\
    //                               FEATURE
    //----------------------------------------------------------------------------//

    public static function sendToSemjuice()
    {
        // api for common
        require SEMJUICE_PLUGIN_DIR . 'api/SemjuiceSdkApi.php';

        // api for WP
        require SEMJUICE_PLUGIN_DIR . 'inc/classes/semjuice-api-custom.php';

        // run
        $api = new Semjuice_Api_Custom();

        $return = $api->siteEdit(array(
            'api_url' => get_option(SEMJUICE_PREFIX . 'api_url'),
            'site_id' => get_option(SEMJUICE_PREFIX . 'site_id')
        ));

        return $return;
    }

    public static function pluginChangeName($plugin_dir, $plugin_file_main)
    {
        self::copyRecursive(WP_PLUGIN_DIR . '/' . $plugin_dir, WP_PLUGIN_DIR . '/' . $_POST['plugin_dir']);

        // deactivate and delete current plugin
        deactivate_plugins($plugin_dir . '/' . $plugin_file_main);
        delete_plugins(array($plugin_dir . '/' . $plugin_file_main));

        // activate new plugin name (manually because activate_plugins call plugin)
        wp_clean_plugins_cache();
        $plugin = plugin_basename($_POST['plugin_dir'] . '/' . $plugin_file_main);
        validate_plugin($plugin);
        wp_register_plugin_realpath( WP_PLUGIN_DIR . '/' . $plugin );

        $current = get_option( 'active_plugins', array() );
        $current[] = $plugin;
        sort($current);
        update_option('active_plugins', $current);
    }

    //----------------------------------------------------------------------------\\
    //                               HELPERS
    //----------------------------------------------------------------------------//

    public static function exitMessage($message)
    {
        echo '<div class="wrap">
            <div id="message" class="updated notice is-dismissible">
                <p style="padding: 10px;">
                    ' . $message . '
                </p>
            </div>
        </div>';
        exit;
    }

    public static function chmodRecursive($path, $chmod) {
        $dir = new DirectoryIterator($path);
        foreach ($dir as $item) {
            chmod($item->getPathname(), $chmod);
            if ($item->isDir() && !$item->isDot()) {
                self::chmodRecursive($item->getPathname(), $chmod);
            }
        }
    }

    public static function copyRecursive($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    self::copyRecursive($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    public static function deleteRecursive($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object)) {
                        self::deleteRecursive($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            rmdir($dir);
        }
    }
}
