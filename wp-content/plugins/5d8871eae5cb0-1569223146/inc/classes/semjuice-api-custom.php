<?php
if (!function_exists('add_action')) {
    // direct access > 404 ...
    header("HTTP/1.0 404 Not Found");
    exit;
}

class Semjuice_Api_Custom extends SemjuiceSdkApi
{
    protected $postParameters = array(
        'post_author'   => null,
    );

    protected function init()
    {
        $this
            ->setApiKey(get_option(SEMJUICE_PREFIX . 'api_key'))
            ->setApiSecret(get_option(SEMJUICE_PREFIX . 'api_secret'));

        $this->postParameters = array(
            'post_author'   => get_option(SEMJUICE_PREFIX . 'post_author'),
        );
    }

    //----------------------------------------------------------------------------\\
    //                               RECEIVE
    //----------------------------------------------------------------------------//

    protected function testAccess()
    {
        return array(
            'status' => 'success',
            'data' => array(
                'version_plugin' => SEMJUICE_VERSION,
                'version_api' => self::API_VERSION
            )
        );
    }

    protected function articleSend($parameters)
    {
        $resApi = $this->request(array(
            'command' => 'netlinkArticleGet',
            'parameters' => array(
                'id' => $parameters['inner_id'],
            )
        ));
        
        if (isset($resApi['status']) && $resApi['status'] == 'success') {
            return $this->articleAdd($resApi['data']);
        }

        return array(
            'status' => 'error',
            'data' => array_merge($this->errorMessage['article_not_add'], array(
                'resultApi' => $resApi,
            ))
        );
    }

    protected function articleAdd($parameters)
    {
        $this->postTagAllowed();
        $postId = wp_insert_post($this->postData($parameters));

        if ($postId instanceof WP_Error || !$postId) {
            return array(
                'status' => 'error',
                'data' => array_merge($this->errorMessage['article_not_add'], array(
                    'error' => $postId,
                    'parameters' => $parameters,
                ))
            );
        }

        $this->postImage($parameters, $postId);

        return array(
            'status' => 'success',
            'data' => array(
                'id' => $postId,
                'url' => get_permalink($postId),
            )
        );
    }

    protected function articleUpdate($parameters)
    {
        $data = $this->articleFind(array('inner_id' => $parameters['inner_id']));

        if (!isset($data['status'])
            || (isset($data['status']) && $data['status'] == 'error')
            || !isset($data['data']['id'])
            || !$data['data']['id']
        ) {
            return array(
                'status' => 'error',
                'data' => $this->errorMessage['article_not_exist']
            );
        }

        $postId = $data['data']['id'];

        $this->postTagAllowed();
        $result = wp_update_post(array_merge(
            array('ID' => $postId),
            $this->postData($parameters)
        ));

        $this->postImage($parameters, $postId);

        return array(
            'status' => $result == $postId ? 'success' : 'error'
        );
    }

    protected function articleFind($parameters)
    {
        $id             = null;
        $url            = null;
        $article_status = null;

        // search in content : https://gist.github.com/hmfs/2287412
        $searchQuery = array(
            'post_type' => 'post',
            'ignore_sticky_posts' => true,
            'post_status' => array('any')
        );
        if (isset($parameters['url']) && $parameters['url']) {
            $searchQuery['s'] = $parameters['url'];
        }
        if (isset($parameters['article_id']) && $parameters['article_id']) {
            $searchQuery['p'] = $parameters['article_id'];
        }
        if (isset($parameters['inner_id']) && $parameters['inner_id']) {
            $searchQuery['meta_key'] = 'semjuice_add';
            $searchQuery['meta_value'] = $parameters['inner_id'];
        }

        $queryContent = new WP_Query($searchQuery);

        while ( $queryContent->have_posts() ) {
            $queryContent->the_post();
            $id             = $queryContent->post->ID;
            $url            = get_permalink($id);
            $article_status = get_post_status($id);
        }

        return array(
            'status' => $url === null ? 'error' : 'success',
            'data' => array(
                'id'             => $id,
                'url'            => $url,
                'article_status' => $article_status == 'publish' ? 1 : 0,
            )
        );
    }

    protected function categoryGet()
    {
        $wpCategories = get_categories(array(
            'taxonomy' => 'category',
            'hide_empty' => 0,
        ));

        $categories = array();
        foreach ($wpCategories as $wpCat) {
            $categories[] = array(
                'id'     => $wpCat->term_id,
                'title'  => $wpCat->name,
                'parent' => $wpCat->parent,
            );
        }

        return array(
            'status' => $categories ? 'success' : 'error',
            'data' => $categories
        );
    }

    protected function infoUpdate($parameters)
    {
        $info = get_option(SEMJUICE_PREFIX . 'info');
        update_option(SEMJUICE_PREFIX . 'info', array_replace_recursive($info, $parameters['info']));

        return array(
            'status' => 'success'
        );
    }

    //----------------------------------------------------------------------------\\
    //                               WP HELPERS
    //----------------------------------------------------------------------------//

    protected function postData($parameters)
    {
        // meta_input : custom field / tag semjuice / other
        $metaInput = isset($parameters['custom_fields']) && is_array($parameters['custom_fields'])
            ? $parameters['custom_fields']
            : array();

        $metaInput['semjuice_add'] = $parameters['inner_id'];

        $postAuthor = $this->postParameters['post_author'];

        if (!$postAuthor) {
            $authors = get_users(array('role' => 'author', 'fields' => array('ID')));
            $keyAuthorRand = array_rand($authors);
            $postAuthor = $authors[$keyAuthorRand]->ID;
            $postAuthor = !$postAuthor ? 1 : $postAuthor;
        }

        return array(
            'post_status'   => $parameters['article_status'] == 1 ? 'publish' : 'draft',
            'post_title'    => $parameters['title'],
            'post_content'  => $parameters['html'],
            'post_category' => array($parameters['category']),
            'post_author'   => $postAuthor,
            'meta_input'    => $metaInput,
        );
    }

    protected function postTagAllowed()
    {
        add_filter('wp_kses_allowed_html', array($this, 'postTagAllowedCallback'), 1, 1); // PHP 5.2 ...
    }

    public function postTagAllowedCallback($allowedposttags)
    {
        // Here add tags and attributes you want to allow
        $allowedposttags['iframe'] = array(
            'align'                 => true,
            'width'                 => true,
            'height'                => true,
            'frameborder'           => true,
            'name'                  => true,
            'src'                   => true,
            'id'                    => true,
            'class'                 => true,
            'style'                 => true,
            'scrolling'             => true,
            'marginwidth'           => true,
            'marginheight'          => true,
            'allowfullscreen'       => true,
            'mozallowfullscreen'    => true,
            'webkitallowfullscreen' => true,
        );
        return $allowedposttags;
    }

    protected function postImage($parameters, $postId)
    {
        if (isset($parameters['image_main']) && $parameters['image_main']) {
            $this->imageMainAdd($parameters, $postId);
        }

        if (isset($parameters['image_second']) && $parameters['image_second']) {
            $this->imageSecondAdd($parameters, $postId);
        }
    }

    protected function imageMainAdd($parameters, $postId)
    {
        require_once(ABSPATH . 'wp-admin/includes/image.php'); // for wp_generate_attachment_metadata()

        $wpUploadDirectory = wp_upload_dir();
        $directory         = wp_mkdir_p($wpUploadDirectory['path'])
            ? $wpUploadDirectory['path']
            : $wpUploadDirectory['basedir'];

        $imageName         = basename($parameters['image_main']['file']);
        $imagePath         = $directory . '/' . $imageName;

        file_put_contents(
            $imagePath,
            file_get_contents($parameters['image_main']['file'])
        );

        $attachId = $this->imageAttachment($imagePath, $imageName, $postId);

        set_post_thumbnail($postId, $attachId);
    }

    protected function imageSecondAdd($parameters, $postId)
    {
        $wpUploadDirectory = wp_upload_dir();

        if (wp_mkdir_p($wpUploadDirectory['path'])) {
            $directory = $wpUploadDirectory['path'];
            $urlBase = $wpUploadDirectory['url'];
        } else {
            $directory = $wpUploadDirectory['basedir'];
            $urlBase = $wpUploadDirectory['baseurl'];
        }

        $images = array();

        foreach ($parameters['image_second'] as $image) {
            $imageName = basename($image['file']);
            $imagePath = $directory . '/' . $imageName;
            $imageUrl  = $urlBase . '/' . $imageName;

            file_put_contents(
                $imagePath,
                file_get_contents($image['file'])
            );

            $this->imageAttachment($imagePath, $imageName, $postId);

            $images[$image['current_uri']] = $imageUrl;
        }

        wp_update_post(array(
            'ID' => $postId,
            'post_content' => str_replace(array_keys($images), array_values($images), $parameters['html'])
        ));
    }

    protected function imageAttachment($imagePath, $imageName, $postId)
    {
        $wp_filetype = wp_check_filetype($imagePath, null);

        $attachment  = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => sanitize_file_name($imageName),
            'post_content'   => '',
            'post_status'    => 'inherit',
        );
        $attachId    = wp_insert_attachment($attachment, $imagePath, $postId);
        $attachData  = wp_generate_attachment_metadata($attachId, $imagePath);
        wp_update_attachment_metadata($attachId, $attachData);

        return $attachId;
    }
}
