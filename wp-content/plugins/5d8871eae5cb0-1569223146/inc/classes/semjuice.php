<?php
if (!function_exists('add_action')) {
    // direct access > 404 ...
    header("HTTP/1.0 404 Not Found");
    exit;
}

class Semjuice
{
    public static function plugin_activation()
    {
        delete_option(SEMJUICE_PREFIX . 'api_url');
        delete_option(SEMJUICE_PREFIX . 'info');
    }

    public static function plugin_deactivation()
    {
        self::delete_options_prefixed(SEMJUICE_PREFIX);
    }

    public static function delete_options_prefixed($prefix)
    {
        global $wpdb;
        $wpdb->query("DELETE FROM {$wpdb->options} WHERE option_name LIKE '{$prefix}%'");
    }
}
