<?php
if (!function_exists('current_user_can')) {
    // direct access > 404 ...
    header("HTTP/1.0 404 Not Found");
    exit;
}
if (!current_user_can('manage_options')) {
    wp_die(__('Sorry, you are not allowed to manage options for this site.'));
}

$datas    = isset($datas)    ? $datas    : array();
$isSubmit = isset($isSubmit) ? $isSubmit : false;



function formSendToSemjuice($datas, $mea = false)
{
    if (!$datas['api_key'] || !$datas['api_secret']) {
        return false;
    }
    ?>
    <?php if ($datas['plugin_dir'] != 'semjuice'): ?>

    <?php if ($mea): ?>
        <br>
        <div style="background: #effaee; padding: 10px;">
    <?php endif; ?>


    <form method="post" action="">
        <?php settings_fields(SEMJUICE_NONCE_CONFIG . '_send'); ?>

        <h2 class="title"><?php _e('Information communiqué à SEMJuice') ?></h2>
            <p><?php _e('Il est nécessaire d\'envoyer au moins une fois ces données à SEMJuice.'); ?></p>

            <table class="form-table">
                <tr>
                    <th><label for="api_url"><?php _e('URL de lien entre SEMJuice et ce blog') ?></label></th>
                    <td>
                        <input name="api_url" type="text" id="api_url" value="<?php echo $datas['api_url']; ?>" class="regular-text" />
                        <br>Merci de tester l'accès :
                        <br><a href="<?php echo $datas['api_url']; ?>" target="_blank">Cliquez ici</a>
                        <br>Vous devez obtenir "Access done", nous communiquerons avec vous à partir de cette URL.
                    </td>
                </tr>
                <tr>
                    <th><label for="site_id"><?php _e('ID de votre site') ?></label></th>
                    <td>
                        <input name="site_id" type="text" id="site_id" value="<?php echo $datas['site_id']; ?>" class="regular-text" />
                        <br>Vous trouverez l'id du site sous le nom de votre site sur le lien suivant :
                        <br><a href="http://www.semjuice.com/admin/site-list" target="_blank">liste de vos sites</a>
                        <br>(vous devez être connecté à votre compte)
                    </td>
                </tr>
            </table>

        <?php if (!$datas['api_key'] || !$datas['api_secret']) { echo 'Vous devez entrer votre clé API avant d\'envoyer à SEMJuice !'; } ?>
        <?php submit_button('Envoyer à SEMJuice'); ?>
        </form>

    <?php if ($mea): ?>
        </div>
    <?php endif; ?>

    <?php endif; ?>

    <?php
}
?>

<div class="wrap">
    <h1><?php _e('SEMJuice v' . SEMJUICE_VERSION . ' : Configuration'); ?></h1>

    <?php if (isset($messageSuccess) && !empty($messageSuccess)): ?>
        <div class="notice notice-success is-dismissible">
        <p style="padding: 10px;">
            <?php _e($messageSuccess); ?>
        </p>
    </div>
    <?php endif; ?>

    <?php if (isset($messageError) && !empty($messageError)): ?>
        <div class="notice notice-error is-dismissible">
        <p style="padding: 10px;">
            <?php _e($messageError); ?>
        </p>
    </div>
    <?php endif; ?>


    <?php if ($datas['plugin_dir'] != 'semjuice'): ?>

        <?php if (isset($datas['info']['data']) && is_array($datas['info']['data']) && $datas['info']['data']): ?>

            <h2 class="title"><?php _e('Information sur votre site') ?></h2>
            <p><?php _e('Information SEMJuice lié à votre site.'); ?></p>

            <table class="wp-list-table widefat plugins">
            <tbody>
                <?php foreach ($datas['info']['data'] as $info): ?>
                    <tr>
                    <th style="width: 200px;"><?php echo $info['title']; ?></th>
                    <td><?php echo $info['value']; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?php endif; ?>

    <?php endif; ?>

    <?php if (isset($datas['info']['data']['error_install'])): ?>
        <?php formSendToSemjuice($datas, true); ?>
    <?php endif; ?>


    <form method="post" action="">
        <?php settings_fields(SEMJUICE_NONCE_CONFIG); ?>

        <?php if ($datas['plugin_dir'] != 'semjuice'): ?>

            <h2 class="title"><?php _e('Lorsque SEMJuice poste un article sur votre site') ?></h2>
            <p><?php _e('Options liées à l\'ajout d\'un article par SEMJuice.'); ?></p>

            <table class="form-table">
            <tr valign="top">
                <th><label for="post_author"><?php _e('Auteur par défaut') ?></th>
                <td><?php wp_dropdown_users(array(
                        'name' => 'post_author',
                        'selected' => $datas['post_author'],
                        'show_option_all' => 'Auteur aléatoire'
                    )); ?></td>
            </tr>
        </table>

            <h2 class="title"><?php _e('API SEMJuice') ?></h2>
            <p><?php _e('Configuration lié à l\'API SEMJuice, lié à votre compte.<br>Vous trouverez votre clé API publique et secrète à cette adresse : <br>
        <a href="http://www.semjuice.com/admin/publication-api">récupérer ma clé API</a> (vous devez être connecté)'); ?></p>

            <table class="form-table">
            <tr>
                <th><label for="api_key"><?php _e('Votre clé publique') ?></label></th>
                <td><input name="api_key" type="text" id="api_key" value="<?php echo $datas['api_key']; ?>" class="regular-text code" /></td>
            </tr>
            <tr>
                <th><label for="api_secret"><?php _e('Votre clé secrète') ?></label></th>
                <td><input name="api_secret" type="text" id="api_secret" value="<?php echo $datas['api_secret']; ?>" class="regular-text code" /></td>
            </tr>
        </table>

            <h2 class="title"><?php _e('Options') ?></h2>
            <p><?php _e('options supplémentaires'); ?></p>

        <?php else: ?>

            <h2 class="title"><?php _e('Sécurité') ?></h2>
            <p><?php _e('Pour éviter de rendre le plugin détectable, il est impératif que vous changiez
            le nom du dossier du plugin (actuellement "semjuice") par un terme non détectable.<br>
            <b>Il vous faudra ensuite réactiver le plugin.</b>'); ?></p>

        <?php endif; ?>

        <table class="form-table">
            <tr>
                <th><label for="plugin_dir"><?php _e('Changer le nom du dossier du plugin') ?></label></th>
                <td>
                    <input name="plugin_dir" type="text" id="plugin_dir" value="<?php echo $datas['plugin_dir'] == 'semjuice' ? uniqid() . '-' . time() : $datas['plugin_dir']; ?>" class="regular-text code" />
                    <br>Si vous changez le nom du plugin, vous devrez réactiver le plugin.
                    <br>Ceci vous permet de rendre non détectable le plugin
                    <br>Chiffre/lettres/Tiret (-) uniquement
                </td>
            </tr>
        </table>

        <?php submit_button(); ?>
    </form>

    <?php if (!isset($datas['info']['data']['error_install'])): ?>
        <?php formSendToSemjuice($datas); ?>
    <?php endif; ?>

</div>
