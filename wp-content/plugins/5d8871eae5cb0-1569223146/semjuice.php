<?php
/*
Plugin Name: SEMJuice plugin
Plugin URI: http://www.semjuice.com/wordpress-plugin/
Description: Automatiser l'insertion des articles SEMJuice sur votre blog
Version: 1.0.4
Author: Jeremy Gallois
Author URI: http://www.semjuice.com
*/

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    // direct access > 404 ...
    header("HTTP/1.0 404 Not Found");
    // echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('SEMJUICE_VERSION', '1.0.4');
define('SEMJUICE_PREFIX', 'semjuice_');
define('SEMJUICE_NONCE_CONFIG', SEMJUICE_PREFIX . 'config');
define('SEMJUICE_PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once(SEMJUICE_PLUGIN_DIR . 'inc/classes/semjuice.php');

register_activation_hook(__FILE__, array('Semjuice', 'plugin_activation')); // PHP 5.4 : not use ::class
register_deactivation_hook(__FILE__, array('Semjuice', 'plugin_deactivation'));

if (is_admin()) {
    require_once(SEMJUICE_PLUGIN_DIR . 'inc/classes/semjuice-admin.php');
    add_action('init', array('Semjuice_Admin', 'init'));
}
