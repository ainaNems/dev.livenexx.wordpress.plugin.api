<?php
/**
 * Plugin Name: WP PBN Express
 * Plugin URI: https://pbn.express/client/plugin
 * Description: Publication Gateway for PBN Express
 * Version: 0.4
 */

if (!defined('ABSPATH')) {
    exit;
}

defined('PBNEXPRESS_PLUGIN_VERSION') or define('PBNEXPRESS_PLUGIN_VERSION', "0.4");
defined('PBNEXPRESS_DEBUG') or define('PBNEXPRESS_DEBUG', true);
defined('PBNEXPRESS_DEV') or define('PBNEXPRESS_DEV', false);
defined('PBNEXPRESS_LOCAL') or define('PBNEXPRESS_LOCAL', false);
defined('PBNEXPRESS_PLUGIN_UPDATE_PATH') or define('PBNEXPRESS_PLUGIN_UPDATE_PATH', 'https://pbn.express/plugin-wp');
defined('PBNEXPRESS_FILE') or define('PBNEXPRESS_FILE', plugin_basename(__FILE__));

$pbnexpress_service = "https://pbn.express/services/v1";
if (PBNEXPRESS_DEV) {
    if (PBNEXPRESS_LOCAL) {
        $pbnexpress_service = "http://localhost:8080/services/v1";
    } else {
        $pbnexpress_service = "https://preprod.pbn.express/services/v1";
    }
}

// add hook for auto_update
add_action('admin_menu', 'pbnexpress_update');
add_action('network_admin_menu', 'pbnexpress_update');
add_filter('plugin_action_links',  'plugin_links', 10, 2);
//add_action('admin_init',  'pbnexpress_show_debug', 100);

add_action('admin_menu', function () {
    //options-general.php?page=show_debug
    $hook = add_options_page('WP PBN Express', 'WP PBN Express', 'manage_options', 'pbn-express-debug-info', 'pbnexpress_show_debug');

    add_action('load-'.$hook, function () {
        add_screen_option('layout_columns', ['default' => 2]);
    });
});


// REST API extension for PBN-Express
add_action('rest_api_init', function () {
    register_rest_route('pbn-express/v1', '/authors', array(
        'methods' => 'GET',
        'callback' => 'pbnexpress_list_authors',
    ));
});

add_action('rest_api_init', function () {
    register_rest_route('pbn-express/v1', '/author/', array(
        'methods' => 'POST',
        'callback' => 'pbnexpress_check_author_credential',
    ));
});

add_action('rest_api_init', function () {
    register_rest_route('pbn-express/v1', '/categories', array(
        'methods' => 'GET',
        'callback' => 'pbnexpress_list_categories',
    ));
});

add_action('rest_api_init', function () {
    register_rest_route('pbn-express/v1', '/post', array(
        'methods' => 'POST',
        'callback' => 'pbnexpress_post_article',
    ));
});

add_action('rest_api_init', function () {
    register_rest_route('pbn-express/v1', '/media', array(
        'methods' => 'POST',
        'callback' => 'pbnexpress_post_medias',
    ));
});

//update the plugin
function pbnexpress_update()
{
    require_once(ABSPATH . 'wp-content/plugins/pbn-express/pbn-express_update.php');
    new pbnexpress_auto_update(PBNEXPRESS_PLUGIN_VERSION, PBNEXPRESS_PLUGIN_UPDATE_PATH, PBNEXPRESS_FILE);
}

//add Link for debug with page=pbn-express-debug-info
function plugin_links($actions, $plugin_file)
{
    $action_links = array(

        'debug' => array(
            'label' => __('Debug', 'pbn-express'),
            'url' => admin_url('options-general.php?page=pbn-express-debug-info')
        ),

        'faq' => array(
            'label' => __('FAQ', 'pbn-express'),
            'url' => 'https://pbn.express/faq'
        )
    );

    return plugin_action_links($actions, $plugin_file, $action_links, 'before');

}

function plugin_action_links($actions, $plugin_file, $action_links = array(), $position = 'after')
{
    static $plugin;
    if (!isset($plugin)) {
        $plugin = plugin_basename(__FILE__);
    }

    if ($plugin == $plugin_file && !empty($action_links)) {
        foreach ($action_links as $key => $value) {

            $link = array($key => '<a href="' . $value['url'] . '">' . $value['label'] . '</a>');
            if ($position == 'after') {
                $actions = array_merge($actions, $link);

            } else {
                $actions = array_merge($link, $actions);
            }
        }//foreach
    }// if
    return $actions;
}

function pbnexpress_debuglog($message)
{
    file_put_contents(__DIR__ . '/debug.log', date('Y-m-d H:i:s') . ' - ' . $message . "<br/>\n", FILE_APPEND);
}

function pbnexpress_var2log($object = null, $msg = '')
{
    ob_start(); // start buffer capture
    var_export($object); // dump the values
    $contents = ob_get_contents(); // put the buffer into a variable
    ob_end_clean(); // end capture
    pbnexpress_debuglog($msg . $contents); // log contents of the result of var_dump( $object )
}

function pbnexpress_output_debuglog()
{
    $log_filename = __DIR__ . '/debug.log';
    if (file_exists($log_filename)) {
        return file_get_contents($log_filename);
    }
    return "no debug.log found";
}

function pbnexpress_getUserAuthentification($username, $artId, $tarId, $nonce)
{
    global $pbnexpress_service;
    $url = $pbnexpress_service . "/user-credential/" . $artId . "/" . $tarId . "/" . $nonce;

    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($url, '$url:');
    }
    global $wp_version;
    $args = array(
        'timeout' => 120,
        'httpversion' => '1.1',
        'user-agent' => 'WordPress-PBN-Express/' . $wp_version . '; ' . home_url(),
        'sslverify' => false,

    );

    $response = wp_remote_get($url, $args);
    if (is_wp_error($response)) {
        $return = array();
        $return["code"] = "no_response_from_pbn_express";
        $return["message"] = "can't reach pbn.express  " . $response->get_error_message();
        $return["data"] = array();
        $return["data"]["status"] = 500;
        wp_send_json($return);
    }

    $body = wp_remote_retrieve_body($response);
    $data = json_decode($body, true);

    $password = base64_decode($data["password"]);
    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($username, '$username:');
        pbnexpress_var2log($password, '$password:');
    }

    $user = apply_filters('authenticate', null, $username, $password);
    //return wp_authenticate($username, $password);
    return $user;
}

function pbnexpress_check_user_check_author_credential(WP_REST_Request $request)
{
    $parameters = $request->get_json_params();

    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($parameters, '$parameters:');
    }

    $username = sanitize_user($parameters["author"]);
    $artId = intval($parameters["article_id"]);
    $tarId = intval($parameters["target_id"]);
    $nonce = $parameters["nonce"];

    if (PBNEXPRESS_DEBUG) {
        pbnexpress_debuglog('$username:' . $username);
        pbnexpress_debuglog('$artId:' . $artId);
        pbnexpress_debuglog('$tarId:' . $tarId);
        pbnexpress_debuglog('$nonce:' . $nonce);
    }

    $user = get_user_by('login', $username);
    if (is_wp_error($user)) {
        $return = array();
        $return["code"] = "no_user_found_with_given_login";
        $return["message"] = "user not found with the login provided " . $user->get_error_message();
        $return["data"] = array();
        $return["data"]["status"] = 500;
        wp_send_json($return);
    }
    $user = pbnexpress_getUserAuthentification($username, $artId, $tarId, $nonce);
    if (is_wp_error($user)) {
        $return = array();
        $return["code"] = "password_does_not_match";
        $return["message"] = "password does not match for user " . $user->get_error_message();
        $return["data"] = array();
        $return["data"]["status"] = 500;
        wp_send_json($return);

    }
    $return = array();
    $return["code"] = "user_exist";
    $return["message"] = "user exist";
    $return["data"] = array();
    $return["data"]["status"] = 200;
    $return["data"]["admin"] = false;
    if (in_array('admin', (array)$user->roles)) {
        $return["data"]["admin"] = true;
    }
    $return["data"]["user_id"] = $user->ID;
    wp_send_json($return);
}

function pbnexpress_list_authors()
{
    $blog_id = 1;
    if (is_multisite()) {
        $blog_id = get_current_blog_id();
    }
    $blogusers = get_users('blog_id=' . $blog_id);
    if (is_wp_error($blogusers)) {
        $return = array();
        $return["code"] = "no_user_list_given";
        $return["message"] = "cannot get list of users " . $blogusers->get_error_message();
        $return["data"] = array();
        $return["data"]["status"] = 500;

        wp_send_json($return);
    }

    $return = array();
    $return["code"] = "list_of_users_ok";
    $return["message"] = "list of authors";
    $return["data"] = array();
    $return["data"]["status"] = 200;
    $return["data"]["authors"] = array();
    // Array of WP_User objects.
    foreach ($blogusers as $user) {
        $return["data"]["authors"][$user->user_login] = $user->ID;
    }
    wp_send_json($return);
}

function pbnexpress_list_categories()
{
    $blogterms = get_terms(array('taxonomy' => 'category', 'hide_empty' => false));
    if (is_wp_error($blogterms)) {
        $return = array();
        $return["code"] = "no_list_of_categories_given";
        $return["message"] = "cannot get list of categories " . $blogterms->get_error_message();
        $return["data"] = array();
        $return["data"]["status"] = 500;
        wp_send_json($return);
    }
    $return = array();
    $return["code"] = "list_of_categories_ok";
    $return["message"] = "list of categories";
    $return["data"] = array();
    $return["data"]["status"] = 200;
    $return["data"]["categories"] = array();
    // Array of WP_Term objects.
    foreach ($blogterms as $term) {
        $return["data"]["categories"][$term->name] = $term->term_id;
    }
    wp_send_json($return);
}

function pbnexpress_post_article(WP_REST_Request $request)
{
    $parameters = $request->get_json_params();
    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($parameters, '$parameters:');
    }
    
    $username = sanitize_user($parameters["author"]);
    $artId = intval($parameters["article_id"]);
    $tarId = intval($parameters["target_id"]);
    $previewId = intval($parameters["preview_id"]);
    $nonce = $parameters["nonce"];

    if (PBNEXPRESS_DEBUG) {
        pbnexpress_debuglog('$username:' . $username);
        pbnexpress_debuglog('$artId:' . $artId);
        pbnexpress_debuglog('$tarId:' . $tarId);
        pbnexpress_debuglog('$nonce:' . $nonce);
    }
    $user = pbnexpress_getUserAuthentification($username, $artId, $tarId, $nonce);

    if (is_wp_error($user)) {
        $return = array();
        $return["code"] = "password_does_not_match";
        $return["message"] = "password does not match for user " . $user->get_error_message();
        $return["data"] = array();
        $return["data"]["status"] = 500;
        wp_send_json($return);
    }

    // Create post object
    $my_post = array(
        'post_title' => wp_strip_all_tags($parameters["title"]),
        'post_content' => $parameters["content"],
        'post_status' => $parameters["status"],
        'post_author' => $user->ID,
        'post_type' => $parameters["post_type"],
        'post_category' => array($parameters["category_id"]),
        'post_date' => $parameters["date"] ,
        'post_date_gmt' => $parameters["date"],
    );

    // Insert the post into the database
    $post_id = wp_insert_post($my_post);
    if (is_wp_error($post_id)) {
        $return = array();
        $return["code"] = "no_publication";
        $return["message"] = "post not created " . $post_id->get_error_message();
        $return["data"] = array();
        $return["data"]["status"] = 500;
        $return["data"]["user_id"] = $user->ID;
        wp_send_json($return);
    }
    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($post_id, '$post_id:');
    }
    if ($previewId != 0) {
        set_post_thumbnail($post_id, $previewId);
    }

    $return = array();
    $return["code"] = "post_published";
    $return["message"] = "article publié";
    $return["data"] = array();
    $return["data"]["status"] = 200;
    $return["data"]["post_id"] = $post_id;
    $return["data"]["user_id"] = $user->ID;
    $return["data"]["admin"] = false;
    if (in_array('admin', (array)$user->roles)) {
        $return["data"]["admin"] = true;
    }
    wp_send_json($return);
}

function pbnexpress_post_medias(WP_REST_Request $request)
{
    global $pbnexpress_service;
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    require_once(ABSPATH . 'wp-admin/includes/file.php');
    require_once(ABSPATH . 'wp-admin/includes/media.php');
    $post_id = 1;
    $posts = get_posts(array(
        'numberposts' => 1,
        'orderby' => 'id',
        'sort_order' => 'asc'
    ));
    if ($posts) {
        $post_id = $posts[0]->ID;
    }
    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($post_id, '$post_id:');
    }
    $parameters = $request->get_json_params();
    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($parameters, '$parameters:');
    }
    //{"images" =>["urlid1","urlid2"],"alts"=> ["title","title"],"nonce" => "wxyg"}
    $return = array();
    $return["code"] = "post_published";
    $return["message"] = "article publié";
    $return["data"] = array();
    $return["data"]["status"] = 200;
    $return["data"]["urls"] = array();
    $images = $parameters["images"];
    $titles = $parameters["titles"];
    $originals = $parameters["originals"];
    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($images, '$images:');
    }
    foreach ($images as $index => $image) {
        if (PBNEXPRESS_DEBUG) {
            pbnexpress_var2log($image, '$image: ');
        }
        $url = sanitize_url($pbnexpress_service . $image);
        if (PBNEXPRESS_DEBUG) {
            pbnexpress_var2log($url, '$url: ');
        }
        $title = $titles[$index];
        if (PBNEXPRESS_DEBUG) {
            pbnexpress_var2log($title, '$title: ');
        }
        $attachment_id = pbnexpress_UploadUrl($url, $title, $index);
        if (is_wp_error($attachment_id)) {
            // There was an error uploading the image.
            $data = array();
            $data["urlid"] = $originals[$index];
            $data["url"] = "";
            $data["file_upload_max_size"] = pbnexpress_file_upload_max_size();
            $data["status"] = $attachment_id->get_error_message();
            $return["data"]["urls"][] = $data;
        } else {
            // The image was uploaded successfully!
            $data = array();
            $data["wp_imageid"] = $attachment_id;
            $data["urlid"] = $originals[$index];
            $data["url"] = wp_get_attachment_url($attachment_id);
            $data["status"] = "200";
            $return["data"]["urls"][] = $data;
        }
    }
    wp_send_json($return);

}

function pbnexpress_UploadUrl($sURL, $title, $index)
{
    $aUploadDir = wp_upload_dir();
    $dImageData = wp_remote_retrieve_body(wp_remote_get($sURL));
    $sURL = urldecode($sURL);
    $iRandom = time() . mt_rand(2000, 9999);
    $sFileName = $iRandom . "-" . basename($sURL);

    if (wp_mkdir_p($aUploadDir['path'])) {
        //  Current set folder in uploads folder like 11 or 12 which are stored month wise
        $sFilePath = $aUploadDir['path'] . '/' . $sFileName;
        //  $sFileURL  = $aUploadDir[ 'url' ] . '/' . $sFileName;
    } else {
        //  Main upload folder
        $sFilePath = $aUploadDir['basedir'] . '/' . $sFileName;
        //  $sFileURL  = $aUploadDir[ 'baseurl' ] . '/' . $sFileName;
    }

    $iFileSize = file_put_contents($sFilePath, $dImageData);

    if ($iFileSize == false) {
        return new WP_Error('no_image_uploaded', "Problème lors de l'upload des images");
    }
    // $upload = array( "FileSize" => $iFileSize, "FileName" => $sFileName, "FilePath" => $sFilePath, "FileURL" => $sFileURL );

    $wp_filetype = wp_check_filetype($sFileName, null);
    if (PBNEXPRESS_DEBUG) {
        pbnexpress_var2log($wp_filetype, '$wp_filetype: ');
    }
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => $title,
        'post_content' => '',
        'post_status' => 'inherit'
    );

    $attach_id = wp_insert_attachment($attachment, $sFilePath, 0);
    if (is_wp_error($attach_id)) {
        return $attach_id;
    }
    $attach_data = wp_generate_attachment_metadata($attach_id, $sFilePath);
    wp_update_attachment_metadata($attach_id, $attach_data);

    return $attach_id;

}

function pbnexpress_file_upload_max_size()
{
    static $max_size = -1;

    if ($max_size < 0) {
        // Start with post_max_size.
        $post_max_size = pbnexpress_parse_size(ini_get('post_max_size'));
        if ($post_max_size > 0) {
            $max_size = $post_max_size;
        }

        // If upload_max_size is less, then reduce. Except if upload_max_size is
        // zero, which indicates no limit.
        $upload_max = pbnexpress_parse_size(ini_get('upload_max_filesize'));
        if ($upload_max > 0 && $upload_max < $max_size) {
            $max_size = $upload_max;
        }
    }
    return $max_size;
}

function pbnexpress_parse_size($size)
{
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
    if ($unit) {
        // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
        return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
    } else {
        return round($size);
    }
}

// catch page page=pbn-express-debug-info and displau debug info
function pbnexpress_show_debug()
{
    require_once(ABSPATH . 'wp-content/plugins/pbn-express/pbn-express_debug.php');
    $installed_plugin_debug = new pbnexpress_plugin_debug;

    $page = isset($_REQUEST['page']) ? sanitize_text_field(urldecode($_REQUEST['page'])) : null;
    if (PBNEXPRESS_DEBUG) {
        pbnexpress_debuglog("pbnexpress_show_debug page=$page");
    }
    if ($page != 'pbn-express-debug-info') {
        return;
    }
    ?>

    <div class="wrap">
        <h2>PBN-Express Debug page</h2>
        <?php echo $installed_plugin_debug->display_debug_info() ?>

    </div>
    <?php

}

?>